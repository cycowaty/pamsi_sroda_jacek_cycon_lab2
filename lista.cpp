

#pragma once
#include "elem.h"
#include <iostream>
#include <time.h>
#include<cstdlib>
using namespace std;
template <typename type>

class lista
{
	// wkaznik na pierwszy element list
	elem<type> *glowa;

public:
	lista(void)
	{
		glowa = NULL;
	}

	// metoda dodajaca wskazana ilosc elementow nie wiekszych od zakresu
	void utworz_los(int, type);

	// metoda dodajaca element na poczatek listy
	void dodaj_pocz(type);

	// metoda dodajaca element na koniec listy
	void dodaj_koniec(type);

	// metoda usuwajaca element z poczatku listy
	void usun_pocz();

	// metoda usuwajaca element z konca listy
	void usun_koniec();

	// metoda wyswietlajaca zawartosc listy
	void wyswietl();


    void usun_wszystko();

};
template <typename type>
void lista<type>::usun_wszystko()
{
    elem<type>* tmp = glowa;
elem<type>* poprzedni = NULL;
    while(tmp!= NULL)
    {
        poprzedni = tmp;
        tmp = poprzedni -> nast;
        free( poprzedni );
    }

glowa=NULL;

}

template <typename type>
void lista<type>::dodaj_pocz(type a)
{
	elem<type>* nowy = new elem<type>; // tworzenie nowego elementu
	nowy->dane = a;        // przypisanie zadanej wartosci
	if(glowa == NULL)	   // sprawdzenie, czy lista nie jest pusta
		glowa = nowy;
	else
	{
		nowy->nast = glowa;
		glowa = nowy;
	}

}
template <typename type>
void lista<type>::dodaj_koniec(type a)
{
	elem<type>* nowy = new elem<type>;
	nowy->dane = a;

	if(glowa == NULL)
		glowa = nowy;
	else
	{
		elem<type>* tmp = glowa;

		while(tmp->nast != NULL)
			tmp=tmp->nast;

		tmp->nast = nowy;
	}

}
template <typename type>
void lista<type>::wyswietl()
{
	for(elem<type> *tmp = glowa; tmp != NULL; tmp=tmp->nast)
		cout << tmp->dane << " ";

	cout << endl;
}



template <typename type>
void lista<type>::utworz_los(int ilosc, type zakres)
{
	srand ( time(NULL) );

	for( int i =0; i < ilosc; ++i)
		dodaj_pocz(rand()%zakres);
}
template <typename type>
void lista<type>::usun_koniec()
{
	if(glowa == NULL)
		cout << "Lista jest pusta !";
	else
	{
		elem<type>* tmp = glowa;
		elem<type>* poprzedni = NULL;

		while(tmp->nast)
		{
			poprzedni = tmp;
			tmp=tmp->nast;
		}

		if(poprzedni == NULL)
		{
			delete glowa;
			glowa = NULL;
		}
		else
		{
			poprzedni->nast = NULL;
			delete tmp;
		}
	}
}



template <typename type>
void lista<type>::usun_pocz()
{
	if(glowa == NULL)
		cout << "Lista jest pusta !";
	if(glowa->nast == NULL)
	{
		delete glowa;
		glowa = NULL;
	}
	else
	{
		elem<type>* tmp = glowa->nast;
		delete glowa;
		glowa = tmp;
	}
}

