
#include "lista.cpp"
#include <time.h>
template <typename type>

class kolejka
{
	// lista pelniaca funkcje stosu
	lista <type> k;

public:
	// metoda dodaje element na koniec kolejki
	void dodaj(type);

	// metoda usuwa pierwszy element z kolejki
	void usun();

	// metoda wyswietla element znajdujacy sie poczatku kolejki
	void wyswietl_pocz();

	// metoda wyswietla wszystkie elementy kolejki
	void wyswietl();

	// metoda zwraca ilosc elementow w kolejce
	//int licz();

	// metoda dodaje losowe elementy do kolejki
	void dodaj_los(int,type);
	void usun_wszystko();

};
template <typename type>
void kolejka<type>::usun_wszystko()
{
	k.usun_wszystko();
}

template <typename type>
void kolejka<type>::dodaj(type a)
{
	k.dodaj_koniec(a);
}

template <typename type>
void kolejka<type>::dodaj_los(int ilosc, type zakres)
{
	 srand ( time(NULL) );
	for( int i =0; i < ilosc; ++i)
	dodaj(rand()%zakres);
}

template <typename type>
void kolejka<type>::usun()
{
	k.usun_pocz();
}

template <typename type>
void kolejka<type>::wyswietl_pocz()
{
	k.wyswietl_pocz();
}

template <typename type>
void kolejka<type>::wyswietl()
{
	k.wyswietl();
}
