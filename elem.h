
#pragma once

template <typename type>
class elem
{
public:

	// Zmienna przechowywujaca wartosc elementu
	type dane;
	// wskazniki do nastepnego  elementu
	elem<type> *nast;

	elem(void)
	{
	// wyzerowanie wskaznikow i zmiennej
	dane = 0;
	nast = 0;
	}
};
